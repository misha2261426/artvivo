footer_block = $('.footer-block')
footer_title = $('.footer-block-title')

for (let i = 0; i < footer_block.length; i++) {
    footer_title[i].addEventListener('click', function(event) {
        if (!footer_block[i].classList.contains('footer-block_active')) {
            footer_block[i].classList.add('footer-block_active')
        } else {
            footer_block[i].classList.remove('footer-block_active')
        }
    });
}

header_block = $('.header-menu-block')
header_title = $('.header-menu-block-title-link')

for (let i = 0; i < header_title.length; i++) {
    header_title[i].addEventListener('click', function(event) {
        for (let j = 0; j < header_block.length; j++) {
            header_title[i].style.removeProperty('href')
        }
        if (!header_block[i].classList.contains('header-menu-block_active')) {
            header_block[i].classList.add('header-menu-block_active')
        } else {
            header_block[i].classList.remove('header-menu-block_active')
        }
    });
}

menu_btn = $('.header-menu-btn')[0]
menu = $('.header-menu')[0]

menu_btn.addEventListener('click', function(event) {
    if (!menu.classList.contains('header-menu_open')) {
        menu.classList.add('header-menu_open')
        menu_btn.classList.add('header-menu-btn_open')
        document.getElementsByTagName('body')[0].style.overflow = 'hidden'
    } else {
        menu.classList.remove('header-menu_open')
        menu_btn.classList.remove('header-menu-btn_open')
        document.getElementsByTagName('body')[0].style.overflow = 'auto'
    }
});

$('.header-add-link_search').on('click', function(event) {
    if (!$('.header-add-link_search').hasClass('header-add-link_search_open')) {
        $('.header-nav').css('opacity', '0')
        $('.header-nav').css('z-index', '-1')
        $('.header-add-link_search').addClass('header-add-link_search_open')
    } else {
        $('.header-nav').css('opacity', '1')
        $('.header-add-link_search').removeClass('header-add-link_search_open')
        $('.header-nav').css('z-index', '0')
    }
    
});

$('.header-nav-item_shop').on('mouseenter', function() {
    $('.header').addClass('header_active')
    $('.header-menu').addClass('header-menu_active')
    $('.header-nav-item_shop').addClass('header-nav-item_shop_active')
});

$('.header').on('mouseleave', function() {
    $('.header').removeClass('header_active')
    $('.header-menu').removeClass('header-menu_active')
    $('.header-nav-item_shop').removeClass('header-nav-item_shop_active')
});