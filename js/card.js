card_nav_item = $('.card-info-nav-item')
card_block = $('.card-info-block-item')

for (let i = 0; i < card_block.length; i++) {
    card_nav_item[i].addEventListener('click', function(event) {
        for (let j = 0; j < card_nav_item.length; j++) {
            card_nav_item[j].classList.remove('card-info-nav-item_active')
            card_block[j].classList.remove('card-info-block-item_active')
        }
        card_nav_item[i].classList.add('card-info-nav-item_active')
        card_block[i].classList.add('card-info-block-item_active')
    });
}