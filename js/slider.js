$('.header-banner-slider').slick({
    infinite: true,
    slidesToShow: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    slidesToScroll: 1,
    swipe: false,
    variableWidth: false,
    arrows: false,
    margin: '0',
    dots: false,
});

$('.header-banner-slider-btn_prev').click(function() {
    $('.header-banner-slider').slick('slickPrev');
});

$('.header-banner-slider-btn_next').click(function() {
    $('.header-banner-slider').slick('slickNext');
});

$('.collection-slider').slick({
    infinite: false,
    slidesToShow: 4,
    slidesToScroll: 2,
    variableWidth: true,
    arrows: true,
    margin: '0',
    dots: true,
    appendDots: $('.collection__dots'),
    appendArrows: $('.collection__arrows'),
    prevArrow: '<div class="collection__arrow collection__arrow_dir_left"></div>',
    nextArrow: '<div class="collection__arrow collection__arrow_dir_right"></div>',
    responsive: [
        {
            breakpoint: 900,
            settings: {
                arrows: false,
                slidesToScroll: 1,
                slidesToShow: 2
            }
        },
    ]
});

$('.media-slider').slick({
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    // variableWidth: true,
    arrows: true,
    dots: true,
    appendDots: $('.media__dots'),
    appendArrows: $('.media__arrows'),
    prevArrow: '<div class="media__arrow media__arrow_dir_left"></div>',
    nextArrow: '<div class="media__arrow media__arrow_dir_right"></div>',
    responsive: [
        {
            breakpoint: 900,
            settings: "unslick",
        },
    ]
});

$('.media-slider_mobile').slick({
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    variableWidth: true,
    arrows: false,
    dots: true,
    appendDots: $('.media_mobile-slider-nav'),
    responsive: [
        {
            breakpoint: 3000,
            settings: "unslick",
        },
        {
            breakpoint: 900,
            margin: '0',
        }
    ]
});

$('.inst-slider').slick({
    infinite: false,
    slidesToShow: 4,
    slidesToScroll: 2,
    variableWidth: true,
    arrows: true,
    margin: '0',
    dots: true,
    appendDots: $('.inst__dots'),
    appendArrows: $('.inst__arrows'),
    prevArrow: '<div class="inst__arrow inst__arrow_dir_left"></div>',
    nextArrow: '<div class="inst__arrow inst__arrow_dir_right"></div>',
    responsive: [
        {
            breakpoint: 900,
            settings: {
                arrows: false,
                slidesToScroll: 1,
                slidesToShow: 2
            }
        }
    ]
});

$('.catalog-content-slider').slick({
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    // variableWidth: true,
    arrows: true,
    dots: true,
    appendDots: $('.media__dots'),
    appendArrows: $('.media__arrows'),
    prevArrow: '<div class="media__arrow media__arrow_dir_left"></div>',
    nextArrow: '<div class="media__arrow media__arrow_dir_right"></div>',
    responsive: [
        {
            breakpoint: 900,

        },
    ]
});

$('.catalog_sub-slider').slick({
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    // variableWidth: true,
    arrows: true,
    dots: true,
    appendDots: $('.media__dots'),
    appendArrows: $('.media__arrows'),
    prevArrow: '<div class="media__arrow media__arrow_dir_left"></div>',
    nextArrow: '<div class="media__arrow media__arrow_dir_right"></div>',
    responsive: [
        {
            breakpoint: 900,

        },
    ]
});

$('.card-slider-vert').slick({
    infinite: false,
    slidesToShow: 5,
    dots: false,
    arrows: false,
    autoplay: false,
    draggable: false,
    infinite: false,
    swipe: false,
    variableWidth: false,
    focusOnSelect: true,
    vertical: true,
    asNavFor: '.card-slider-hor',
});

$('.card-slider-hor').slick({
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    variableWidth: false,
    arrows: true,
    dots: true,
    appendDots: $('.media__dots'),
    appendArrows: $('.media__arrows'),
    prevArrow: '<div class="media__arrow media__arrow_dir_left"></div>',
    nextArrow: '<div class="media__arrow media__arrow_dir_right"></div>',
    asNavFor: '.card-slider-vert',
});

$('.card-seen-slider').slick({
    infinite: false,
    slidesToShow: 4,
    slidesToScroll: 2,
    variableWidth: true,
    arrows: true,
    margin: '0',
    dots: true,
    appendDots: $('.inst__dots'),
    appendArrows: $('.inst__arrows'),
    prevArrow: '<div class="inst__arrow inst__arrow_dir_left"></div>',
    nextArrow: '<div class="inst__arrow inst__arrow_dir_right"></div>',
    responsive: [
        {
            breakpoint: 900,
            settings: {
                arrows: false,
                slidesToScroll: 1,
                slidesToShow: 2
            }
        }
    ]
});