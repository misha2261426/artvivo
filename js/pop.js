body = $('body')[0]
pop = $('.pop')[0]
pop_bg = $('.pop_bg')[0]
pop_open = $('.header-add-link_account')[0]
pop_close = $('.pop_close')
pop_enter = $('.pop-block_enter')[0]
pop_reg = $('.pop-block_reg')[0]
pop_reg_btn = $('.pop-block-reg-btn')[0]
pop_reg_enter = $('.reg-block-reg-btn')[0]
pop_forgot = $('.pop-block_forgot')[0]
pop_forgot_btn = $('.pop-block-add-forgot')[0]
pop_sub = $('.pop-block_sub')[0]


pop_open.addEventListener('click', function() {
    pop.classList.remove('pop_hidden')
    pop_bg.classList.remove('pop_hidden')
    body.style.overflow = "hidden"
    pop_enter.classList.remove('pop_hidden')
});

for (let i = 0; i < pop_close.length; i++) {
    pop_close[i].addEventListener('click', function() {
        pop.classList.add('pop_hidden')
        pop_bg.classList.add('pop_hidden')
        body.style.overflow = "visible"
        pop_reg.classList.add('pop_hidden')
        pop_enter.classList.remove('pop_hidden')
        pop_forgot.classList.add('pop_hidden')
        pop_sub.classList.add('pop_hidden')
    });   
};

pop_reg_btn.addEventListener('click', function() {
    pop_reg.classList.remove('pop_hidden')
    pop_enter.classList.add('pop_hidden')
});

pop_reg_enter.addEventListener('click', function() {
    pop_reg.classList.add('pop_hidden')
    pop_enter.classList.remove('pop_hidden')
});

pop_forgot_btn.addEventListener('click', function() {
    pop_forgot.classList.remove('pop_hidden')
    pop_enter.classList.add('pop_hidden')
});